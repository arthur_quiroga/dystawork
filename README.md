DYSTAWORK
=========
Dystawork est un petit framework écrit en php avec une architecture MVC et n-tiers.
Il se veut dynamique et statique afin de pouvoir limiter le temps d'exécution du serveur.



Fonctionnalités
===============
- Gestion de plusieurs projets
- Concept de "bundle" afin de pouvoir exporter des modules d'un projet à un autre
- Utilisation du moteur TWIG 2
- Routeur avec définition des routes dans un fichier de configuration


Fonctionnalités à venir
=======================
- Choix entre requêtes sql écritent en sql ou bien en php
- Ajout de SASS avec eventuellement compilation via php
- Ajout du choix de "compilation" pour du statique
- Simplification des sessions
- Sécurisation de formulaires
- Exportation d'un projet en archive phar