<?php

namespace Dystawork;

final class FileLoader {

    public static function register() {
        spl_autoload_register(array(__CLASS__, 'load'));
    }

    static function load($class) {
        require '..'.DIRECTORY_SEPARATOR.'app'.
        DIRECTORY_SEPARATOR.'settings.php';
        if (strstr($class, 'Twig'))
            return;
        if (!strstr($class, ucfirst(strtolower($settings['project'])).'\\') && !strstr($class, 'Dystawork\\'))
            return;

        $class = str_replace('\\', DIRECTORY_SEPARATOR, $class);
        $class = implode(
            array_map(array(__CLASS__, 'setUcfirst'), explode(DIRECTORY_SEPARATOR, $class)),
            DIRECTORY_SEPARATOR
        );
        $names = explode(DIRECTORY_SEPARATOR, $class);
        if ($names[0] === 'dystawork') {
            $names[0] = dirname(__DIR__, 1).DIRECTORY_SEPARATOR
                .$names[0];
        } else {
            if ($names[1] === 'controller') {
                $names[0] = dirname(__DIR__, 2).DIRECTORY_SEPARATOR.'src'
                .DIRECTORY_SEPARATOR.$names[0].DIRECTORY_SEPARATOR.$names[1];
                $names[1] = $names[2].'.controller';
                unset($names[2]);
            } else if ($names[1] === 'model') {
                $names[0] = dirname(__DIR__, 2).DIRECTORY_SEPARATOR.'src'
                .DIRECTORY_SEPARATOR.$names[0].DIRECTORY_SEPARATOR.$names[1];
                $names[1] = $names[2].'.model';
                unset($names[2]);
            } else if ($names[1] === 'entity') {
                $names[0] = dirname(__DIR__, 2).DIRECTORY_SEPARATOR.'src'
                .DIRECTORY_SEPARATOR.$names[0].DIRECTORY_SEPARATOR.$names[1];
                $names[1] = $names[2].'.entity';
                unset($names[2]);
            }
        }

        $names[count($names)-1] = self::parseFilename($names[count($names)-1]);
        $class = implode($names, DIRECTORY_SEPARATOR);


        $class = self::loadFile($class);


        if (!is_null($class))
            require_once $class;
    }

    private static function loadFile($filePath) {
        $filePath .= '.php';
        if (is_file($filePath)) {
            return $filePath;
        }
        return null;
    }

    private static function parseFilename($filename) {
        if (ctype_lower($filename[1]))
            return $filename;
        switch ($filename[0]) {
            case 'T': $filename .= '.trait'; break;
            case 'I': $filename .= '.interface'; break;
            case 'A': $filename .= '.abstract'; break;
        }
        return substr($filename, 1);
    }

    private static function setUcfirst($str) {
        return lcfirst($str);
    }
}

FileLoader::register();

?>
