<?php

namespace Dystawork\Core;

use Dystawork\Core\Database;

class Entity {

    protected $table = null;
    protected $sql_insert = null;
    protected $sql_select = null;

    final public static function create(string $name, ...$args) {
        $nameArray = explode(':', $name);
        if (count($nameArray) !== 2) {
            throw new \ErrorException('Entity name \''.$name.'\' invalid');
        }

        $name = $nameArray[0].'\\Entity\\'.$nameArray[1];
        return new $name(...$args);
    }

    final public function insert() {
        $class = $this->table;
        // $class = strtolower($class[count($class) - 1]);

        $values = get_object_vars($this);
        if (is_numeric($values['id']))
            return;
        unset($values['id']);
        unset($values['table']);
        foreach ($values as $key => &$value) {
            $value = '\''.$value.'\'';
        }
        $keys = array_keys($values);
        $this->addPrefix($keys, $class);

        $column = implode($keys, ', ');
        $valuesStr = implode($values, ', ');

        $sql = 'INSERT INTO '.$class.'('.$column.') VALUES('.$valuesStr.');';

        $this->executeQuery($sql);
    }

    final public function update() {
        $class = $this->table;
        // $class = strtolower($class[count($class) - 1]);

        $update = '';
        $values = get_object_vars($this);
        if (!is_numeric($values['id']))
            return;
        foreach ($values as $key => $value) {
            if ($key === 'id') {
                continue;
            }
            if ($key == 'table')
                continue;
            if ($update !== '')
                $update .= ', ';

            $update .= $key.'_'.$class.'='.'\''.$value.'\'';
        }

        $sql = 'UPDATE '.$class.' SET '.$update.' WHERE
        id_'.$class.' = \''.$values['id'].'\';';
        $this->executeQuery($sql);
    }

    final public function delete() {
        $class = $this->table;
        // $class = strtolower($class[count($class) - 1]);

        $values = get_object_vars($this);
        if (!is_numeric($values['id']))
            return;

        $sql = 'DELETE FROM '.$class.' WHERE
        id_'.$class.' = \''.$values['id'].'\';';
        echo $sql;
        $this->executeQuery($sql);
    }

    final private function addPrefix(&$keys, $classname) {
        foreach ($keys as &$key) {
            if (strpos($key, 'fk_') === 0) {
                continue;
            }
            $key .= '_'.$classname;
        }
    }

    final public static function getOneBy(string $name, Array $data) {
        $instance = self::create($name);

        if ($instance->sql_select != null) {
            var_dump($instance->sql_select);
            $sql = file_get_contents(ROOT.'/src/project/sql/' + $instance->sql_select);
        } else {
            $class = $instance->table;
            // $class = strtolower($class[count($class) - 1]);

            $update = '';
            $values = get_class_vars(get_called_class());
            foreach ($data as $key => $value) {
                if ($update !== '')
                $update .= ' AND ';
                if (strpos($key[0], 'fk_') === 0)
                $update .= substr($key, 1).'='.'\''.$value.'\'';
                else
                $update .= $key.'_'.$class.'='.'\''.$value.'\'';
            }

            $sql = 'SELECT * FROM '.$class.' WHERE '.$update.';';
        }

        $res = self::executeQuery($sql);

        if ($res === null || $res === false || count($res) === 0)
            return false;

        $res = $res[0];

        foreach ($res as $key => $val) {
            $key = str_replace('_'.$class, '', $key);
            $instance->$key = $val;
        }
        return $instance;
    }

    final public function getManyBy(string $name, Array $data) {
        $instance[] = self::create($name);

        $class = $instance[0]->table;
        // $class = strtolower($class[count($class) - 1]);

        $update = '';
        $values = get_class_vars(get_called_class());
        foreach ($data as $key => $value) {
            if ($update !== '')
                $update .= ' AND ';
            if (strpos($key, 'fk_') === 0)
                $update .= $key.'='.'\''.$value.'\'';
            else
                $update .= $key.'_'.$class.'='.'\''.$value.'\'';
        }

        $sql = 'SELECT * FROM '.$class.' WHERE '.$update.';';
        $res = self::executeQuery($sql);

        if ($res === null || $res === false)
            return false;

        if (count($res) === 0)
            return [];

        for ($k = 0; $k < count($res); ++$k) {
            $inst = null;
            if ($k > 0) {
                $instance[] = self::create($name);
            }
            foreach ($res[$k] as $key => $val) {
                $key = str_replace('_'.$class, '', $key);
                $instance[$k]->$key = $val;
            }
        }

        return $instance;
    }

    final private static function executeQuery($sql) {
        $db = Database::getInstance();
        $db->connect();
        $db->setQuery($sql);
        return $db->execute();
    }

}


?>
