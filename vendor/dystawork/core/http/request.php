<?php

namespace Dystawork\Core\Http;

final class Request {

    public static function get($type) {
    	$res = explode('/', $_SERVER[strtoupper($type)]);
    	for ($k = count($res) - 1; $k > 1; --$k) {
    		if ($res[$k] === '') {
    			unset($res[$k]);
    		} else {
    			break;
    		}
    	}
        return $res;
    }

}

?>
