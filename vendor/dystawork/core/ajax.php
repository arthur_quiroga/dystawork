<?php

namespace Dystawork\Core;

final class Ajax {

    public static function render($code, $html, $other = []) {
        return json_encode([
            'code' => $code,
            'html' => $html,
            'other' => $other
        ]);
    }

}

?>
