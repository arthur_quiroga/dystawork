<?php

namespace Dystawork\Core;

class View {

    private $twig = null;
    private $template = null;

    public function __construct($filename) {
        $this->loadTwig();
        $this->template = $this->twig->load($filename);
    }

    public function render($data) {
        require ROOT.DS.'app'.DS.'template'.DS.'template.php';

        return $this->template->render(
            array_merge($data, $template_var)
        );
    }

    private function loadTwig() {
        // \Twig_Autoloader::register();
        // require_once ROOT.'/vendor/autoload.php';

        $loader = new \Twig_Loader_Filesystem(ROOT.DS);
        $this->twig = new \Twig_Environment($loader, array(
            'cache' => false,
            // 'cache' => ROOT.'/var/cache',
            'debug' => true
        ));
    }


    // extract($data);
    // ob_start();
    // require ROOT.DS.'src'.DS.$view[0].DS.'view'.DS.$view[1].'.view.php';
    // $view = ob_get_clean();
    //
    // if ($this->template) {
    //     ob_start();
    //     require ROOT.DS.'app'.DS.'template.view.php';
    //     return ob_get_clean();
    // }

}

?>
