<?php

namespace Dystawork\Core;

trait Singleton {

    private static $instance = null;

    final public static function getInstance(...$args) {
        if (self::$instance === null)
            self::$instance = new static(...$args);
        return self::$instance;
    }

    final public function __clone() {}
}



?>
