<?php 

namespace Dystawork\Core;

use Dystawork\Core\Database;

final class QueryBuilder {

    private $operation = null;
    private $table = null;
    private $where = null;
    private $join = null;
    private $limit = null;
    private $values = [];
    private $select = null;

    private function __constructor($table, $operation) {
    	$this->table = $table;
    	$this->operation = $operation;
    }

    final public static function insert($table) {
    	return new QueryBuilder($table, 'insert');
    }

    final public static function delete($table) {
    	
    	return new QueryBuilder($table, 'delete');
    }

    final public static function update($table) {	
    	
    	return new QueryBuilder($table, 'udpate');
    }

    final public static function getOne($table, $select) {
    	$this->select = $select;
    	return new QueryBuilder($table, 'select');
    }

    final public static function getMany($table, $select) {
    	$this->select = $select;
    	return new QueryBuilder($table, 'select');
    }

    final public function values($values) {
    	$this->values = $values;
    	return $this;
    }

	/**
     * 
     */
    final public function where($condition) {
        $where = $condition;

        return $this;
    }

    /**
     * 
     */
    final public function join() {

        return $this;
    }

    /**
     * 
     */
    final public function leftJoin() {
        
        return $this;
    }

    /**
     * 
     */
    final public function rightJoin() {
        
        return $this;
    }

    final public function limit($offset, $number) {
    	$limit = $offset.','.$number;
        return $this;
    }

    /**
     * Return the entity(ies).
     * @return
     */
    final public function execute() {
        $request = $operation;

        if ($operation === 'insert') {
        	$val = '';
        	foreach ($this->values as $key => $value) {
        		if ($val !== '')
        			$val .= ',';
        		$val .= $key.'='.$value;
        	}
        	$request .= ' into '.$table . ' values('.$val.')';
        } else if ($operation === 'select') {
        	$val = implode(',', $this->select);
        	$request .= ' '.$val.' from '.$table;
        } else if ($operation === 'update') {
        	$request .= ' '.$table.' SET ';
        	$val = '';
        	foreach ($this->values as $key => $value) {
        		if ($val !== '')
        			$val .= ',';
        		$val .= $key.'='.$value;
        	}
        	$request .= $val;
        }

        if ($where !== '')	 
            $request .= ' WHERE '.$where;
        if ($limit !== '')
            $request .= ' LIMIT '.$limit;

        $res = $this->executeQuery($);

    }

    final private static function executeQuery($sql) {
        $db = Database::getInstance();
        $db->connect();
        $db->setQuery($sql);
        return $db->execute();
    }
}

?>