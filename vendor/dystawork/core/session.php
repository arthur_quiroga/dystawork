<?php

namespace Dystawork\Core;

/**
 *
 */
final class Session {

    public function __construct() {}

    public function get($name) {
        if (isset($_SESSION[$name]))
            return $_SESSION[$name];

        return null;
    }

    public function add($tag, $value) {
        $_SESSION[$tag] = $value;
    }

    public function remove() {
        session_unset();
    }
}


?>
