<?php

namespace Dystawork\Core;

use Dystawork\Extra\DebugTool;
use Dystawork\Core\Ajax;

class Controller {

    private $template = false;
    protected $session = null;

    public function __construct() {
        $this->session = new Session();
    }

    protected function render(string $view, array $data = []) {
        $view = explode(':', $view);
        if (count($view) !== 2) {
            throw new ErrorException('The view \''.$view.'\'is invalid');
        }

        $view = new View('src'.DS.$view[0].DS.'view'.DS.$view[1].'.html.twig');

        DebugTool::stopTime();
        $arr = DebugTool::toArray();

        return $view->render(array_merge($arr, $data), $this->template);
    }

    protected function createModel(string $model) {
        $model = '\\Model\\'.$model;
        return new $model();
    }

    protected function setTemplate(bool $template) {
        $this->template = $template;
    }

}


?>
