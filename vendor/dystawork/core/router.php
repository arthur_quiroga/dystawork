<?php

namespace Dystawork\Core;

use Dystawork\Core\Http\Request;


final class Router {

    private $routes = [];

    public function __construct() {}

    public function addRoute($route, $callback) {
        $this->routes[$route] = $callback;
    }

    public function run() {
        require '..'.DIRECTORY_SEPARATOR.'app'.
        DIRECTORY_SEPARATOR.'settings.php';
        $uri = Request::get('request_uri');

        foreach ($this->routes as $key => $callback) {
            if (($res = $this->isValidUri($uri, explode('/', $key))) !== null) {
                if (is_string($callback['path'])) {
                    $callback = explode('::', $callback['path']);
                    $ns = explode(':', $callback[0]);
                    $class = '\\'.$ns[0].'\\Controller'.'\\'.$ns[1];
                    $method = $callback[1];
                    return (new $class())->$method(...$res);
                    // return ('\\Controller\\'.$callback)(...$res);
                }
                return $callback(...$res);
            }
        }

        $callback = explode('::', ucfirst(strtolower($settings['project'])).':Error::code404');
        $ns = explode(':', $callback[0]);
        $class = '\\'.$ns[0].'\\Controller'.'\\'.$ns[1];
        $method = $callback[1];
        return (new $class())->$method(isset($_POST['type']) && $_POST['type'] === 'ajax');
        // throw new \Exception('Route "'.implode($uri, '/').'" not found');
    }


    private function isValidUri($uri, $route) {
        if (count($uri) != count($route))
            return null;
        if (count($uri) === 0 && count($route) === 0)
            return [];

        $match = [];
        if (preg_match("/{(.*)}/",$route[0], $match) === 1) {

            if (($res = $this->isValidUri(array_slice($uri, 1), array_slice($route, 1))) !== null){
                return array_merge([$uri[0]], $res);
            }

            return null;
        }


        if ($uri[0] != $route[0])
            return null;

        if (($res = $this->isValidUri(array_slice($uri, 1), array_slice($route, 1))) !== null)
            return $res;

        return null;
    }

}

?>
