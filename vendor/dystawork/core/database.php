<?php

namespace Dystawork\Core;

use Dystawork\Core\Singleton;


final class Database {

    private $dbc = null;
    private $sth = null;

    use Singleton;

    public function connect() {
        // If already connected
        if ($this->dbc !== null)
            return;

        require_once ROOT.DS.'app'.DS.'config'.DS.'database.conf.php';

        $host = 'mysql:host='.$database['host'].';dbname='.$database['database'];
        $this->dbc = new \PDO(
            $host,
            $database['user'],
            $database['password'], [
			\PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
			\PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
			\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_EMULATE_PREPARES => false,
		]);
    }

    /**
     * @todo Diconnection of database
     */
    public function disconnect() {

    }

    public function setQuery($sql) {
        $this->sth = $this->dbc->prepare($sql);
    }

    public function execute() {
        if ($this->sth === null || $this->sth === false) {
            return null;
        }

        $this->sth->execute();

        $res = $this->sth->fetchAll();
        // Case when no result is found
        if ($res === false || $res === null) {
            return null;
        }
        // Case when one row is returned
        // if (count($res) === 1) {
        //     return $res[0];
        // }
        return $res;
    }
}

?>
