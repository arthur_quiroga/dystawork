<?php

namespace Dystawork;

use Dystawork\Extra\DebugTool;
use Dystawork\Core\Router;

class Application {

    private $router = null;

    final public function __construct() {
        define('DS', DIRECTORY_SEPARATOR);
        define('ROOT', dirname(__DIR__, 2));

        DebugTool::startTime();

        session_start();

        $this->router = new Router();
    }

    final public function run() {
        echo $this->router->run();
    }


    final public function post($route, $callback) {

    }

    final public function get($route, $callback) {
        $this->router->addRoute($route, $callback);
    }

    final public function put($route, $callback) {

    }

    final public function delete($route, $callback) {

    }

    final public function patch($route, $callback) {

    }

}

?>
