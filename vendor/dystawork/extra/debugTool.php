<?php

namespace Dystawork\Extra;

class DebugTool {

    private static $beginTime = 0;
    private static $endTime = 0;

    public static function startTime() {
        $d = new \DateTime();
        self::$beginTime = round(microtime(true) * 1000);
    }

    public static function stopTime() {
        $d = new \DateTime();
        self::$endTime = round(microtime(true) * 1000);
    }

    public static function addDatabseQuery() {

    }

    public static function toArray() {
        return [
            'developer' => [
                'response_code' => http_response_code(),
                'time' => self::$endTime - self::$beginTime
            ]
        ];
    }

}

?>
