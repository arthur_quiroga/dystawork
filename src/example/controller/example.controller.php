<?php

namespace Example\Controller;

use Dystawork\Core\Controller;
use Dystawork\Core\Entity;

class Example extends Controller {

    public function index() {

        $this->setTemplate(true);

        return $this->render('example:example', [

        ]);
    }

}

?>
