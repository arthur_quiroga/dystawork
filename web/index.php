<?php

ini_set('display_errors', 1);

require_once __DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'vendor'.
    DIRECTORY_SEPARATOR.'dystawork'.DIRECTORY_SEPARATOR.'autoload.php';

require_once __DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR
    .'vendor/autoload.php';

$app = new \Dystawork\Application();


require_once __DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'app'.
    DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'router.conf.php';

foreach ($router as $url => $method) {
    $app->get($url, $method);
}


$app->run();

?>
